

# Dump of table task
# ------------------------------------------------------------

DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `user_author_id` int(11) NOT NULL,
  `user_assign_id` int(11) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `parent_type` enum('SS','EE','SE','ES') DEFAULT NULL,
  `parent_delay` smallint(6) NOT NULL DEFAULT '0' COMMENT 'days',
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;

INSERT INTO `task` (`id`, `date_start`, `date_end`, `user_author_id`, `user_assign_id`, `parent_id`, `parent_type`, `parent_delay`, `title`)
VALUES
  (1,'2015-10-07 10:00:00','2015-10-17 10:00:00',1,2,0,NULL,0,'First task. NoParent'),
  (2,'2015-10-08 10:00:00','2015-10-22 10:41:40',1,3,1,'SS',1,'SS to first +1 day'),
  (3,'2015-10-05 10:41:40','2015-10-22 10:00:00',2,1,1,'EE',5,'task EE to 1+5'),
  (6,'2015-10-17 10:00:00','2015-10-25 10:41:40',0,NULL,1,'ES',0,'ES to first'),
  (9,'2015-10-05 10:41:40','2015-10-17 10:00:00',2,1,1,'EE',0,'task EE to 1+0'),
  (10,'2015-10-05 10:00:00','2015-10-17 10:00:00',0,NULL,6,'SE',0,'SE to 6(es-to-first)');

/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `name`)
VALUES
  (1,'Юзер 1'),
  (2,'Юзер 2'),
  (3,'Юзер 3');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;





# ----



DROP FUNCTION IF EXISTS hierarchy_connect_by_parent_eq_prior_id;
DELIMITER $$
CREATE FUNCTION hierarchy_connect_by_parent_eq_prior_id(value INT) RETURNS INTEGER
NOT DETERMINISTIC
READS SQL DATA
BEGIN
    DECLARE _parent INT;
    DECLARE _rank INT;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET @id = NULL;

    SET _parent = @id;
    SET _rank = 0;

    IF @id IS NULL THEN
            RETURN NULL;
    END IF;

    LOOP
        SET @innerrank = 0;
        SELECT p.id
        INTO   @id
        FROM   (
                SELECT   id, @innerrank := @innerrank+1 AS rank, title
                FROM     task
                WHERE    COALESCE(parent_id, 0) = _parent
                ORDER BY date_start
                ) p
        WHERE   p.rank > _rank LIMIT 0, 1;
        IF @id IS NOT NULL OR _parent = @start_with THEN
                SET @level = @level + 1;
                RETURN @id;
        END IF;
        SET @level := @level - 1;
        SET @innerrank = 0;
        SELECT COALESCE(p.parent_id, 0), p.rank
        INTO   _parent, _rank
        FROM   (
                SELECT id, parent_id, @innerrank := @innerrank+1 AS rank
                FROM    task
                WHERE   COALESCE(parent_id, 0) = (
                    SELECT COALESCE(parent_id, 0) FROM task WHERE id = _parent
                    )
                ORDER BY date_start
               ) p
        WHERE p.id = _parent;
    END LOOP;
END;
$$
DELIMITER ;

