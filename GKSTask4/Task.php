<?php
$db = new mysqli('localhost', 'root', 'root', 'test');

/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 30.10.15
 * Time: 13:09
 */
class Task
{
    //Types:

    //SS    Start-Start
    //EE    End-End
    //SE    Start-End задача 2 стартует после окончания первой
    //ES    End-Start задача 2 заканчивается там где начинается первая

    protected $id;
    protected $date_start;
    protected $date_end;
    protected $user_author_id;
    protected $user_assign_id;
    protected $parent_id;
    protected $parent_type;
    protected $parent_delay;
    protected $title;


    public function __construct($id)
    {
        $this->id = (int)$id;
    }

    protected function loadTask()
    {
        global $db;
        $res = $db->query("SELECT * FROM `task` WHERE `id` = " . $this->id);
        $row = $res->fetch_assoc();
        foreach ($row as $field => $value) {
            $this->$field = $value;
        }
    }

    /**
     * @param $parent_delay Int
     */
    protected function setParentDelay($parent_delay){
        $this->parent_delay = (int)$parent_delay;
    }


    public function setDate($date, $type = 'start', $fromParent = false)
    {
        echo "<b>".$this->id."</b> setDate('".$date."'', '".$type."', ".$fromParent.")<br />";

        global $db;
        if ($type != 'start' && $type != 'end') {
            die("Wrong type");
        }

        $d = new DateTime($date);

        if ($fromParent) {
            //Delay
            $d->add(new DateInterval('P' . $this->parent_delay . 'D'));
            $date = $d->format('Y-m-d H:i:s');
        }

        //Change current task
        $res = $db->query("UPDATE `task` SET `date_" . $type . "` = '" . $date . "' WHERE `id`=" . $this->id);


        $this->loadTask();

        //Select child tasks
        $res = $db->query("SELECT id, parent_type, parent_delay FROM `task` WHERE `parent_id`=" . $this->id . "");
        while ($row = $res->fetch_assoc()) {
            $t = new Task($row['id']);
            $t->setParentDelay($row['parent_delay']);

            //Дети со мной стартуют
            if ($row['parent_type'] == 'SS') {
                $t->setDate($this->date_start, 'start', true);
            }

            //Я стартую после детей. меняем детям end на наш старт
            if ($row['parent_type'] == 'SE') {
                $t->setDate($this->date_start, 'end', true);
            }

            //Детям такой же энд.
            if ($row['parent_type'] == 'EE') {
                $t->setDate($this->date_end, 'end', true);
            }

            //Дети стартуют после моей дэйтEnd;
            if ($row['parent_type'] == 'ES') {
                $t->setDate($this->date_end, 'start', true);
            }

        }

    }

    /**
     * Запрос получения всех связаных. В mysql это не так просто)
     * @param $id
     * @return mixed
     */
    public static function getTree($id)
    {
        $sql = "
          SELECT  CONCAT(REPEAT('    ', level - 1), CAST(id AS CHAR)) as id,
            parent_id,
            level, title, date_start, date_end, parent_type, parent_delay
    FROM    (
        SELECT  id, parent_id, IF(ancestry, @cl := @cl + 1, level + @cl) AS level, title, date_start, date_end, parent_type, parent_delay
            FROM    (
                SELECT  TRUE AS ancestry, _id AS id, parent_id, level, title, date_start, date_end, parent_type, parent_delay
                    FROM    (
                        SELECT  @r AS _id,
                                    (
                                    SELECT  @r := parent_id
                                    FROM    task
                                    WHERE   id = _id
                                    ) AS parent_id,
                                    @l := @l + 1 AS level, title, date_start, date_end, parent_type, parent_delay
                            FROM    (
                                SELECT  @r := " . $id . ",
                                            @l := 0,
                                            @cl := 0
                                    ) vars,
                                    task h
                            WHERE   @r <> 0
                            ORDER BY
                                    level DESC
                            ) qi
                    UNION ALL
                    SELECT  FALSE, hi.id, parent_id, level, hi.title, hi.date_start, hi.date_end, hi.parent_type, hi.parent_delay
                    FROM    (
                        SELECT  hierarchy_connect_by_parent_eq_prior_id(id) AS id, @level AS level, title, date_start, date_end, parent_type, parent_delay
                            FROM    (
                                SELECT  @start_with := " . $id . ",
                                            @id := @start_with,
                                            @level := 0
                                    ) vars, task
                            WHERE   @id IS NOT NULL
                            ) ho
                    JOIN    task hi
                    ON      hi.id = ho.id
                    ) q
            ) q2
        ";
        global $db;
        $res = $db->query($sql);
        if(!$res) echo $db->error;

        return $res->fetch_all(MYSQLI_ASSOC);
    }

}