<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 30.10.15
 * Time: 11:10
 */

ini_set("display_errors",1);

$db = new mysqli('localhost','root','root','test');


$res = $db->query("SELECT * FROM `task` ORDER BY `date_start` LIMIT 100 ");
require 'Task.php';



function getDependsItem($id){

    $tree = Task::getTree($id);
    foreach($tree as $node){

        echo str_pad('',$node['level']*7,'-&nbsp;');
        if($node['id'] == $id ){
            echo '<b>'.$node['id'].':</b> ';
        } else {
            echo ''.$node['id'].'';
        }
        print_r($node); echo '<br />';
    }
}


?>
<html>
<head>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script type="text/javascript">
        google.load("visualization", "1.1", {packages:["gantt"]});
        google.setOnLoadCallback(drawChart);

        function daysToMilliseconds(days) {
            return days * 24 * 60 * 60 * 1000;
        }

        function drawChart() {

            var data = new google.visualization.DataTable(

            );
            data.addColumn('string', 'Task ID');
            data.addColumn('string', 'Task Name');
            data.addColumn('date', 'Start Date');
            data.addColumn('date', 'End Date');
            data.addColumn('number', 'Duration');
            data.addColumn('number', 'Percent Complete');
            data.addColumn('string', 'Dependencies');

            data.addRows([
                    <?php
            while ($row = $res->fetch_assoc()) {

                // assumes dates are patterned 'yyyy-MM-dd hh:mm:ss'
                preg_match('/(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2}):(\d{2})/', $row['date_start'], $match);
                $year = (int) $match[1];
                $month = (int) $match[2] - 1; // convert to zero-index to match javascript's dates
                $day = (int) $match[3];
                $hours = (int) $match[4];
                $minutes = (int) $match[5];
                $seconds = (int) $match[6];

                $start = " new Date($year, $month, $day, $hours, $minutes, $seconds)";


                preg_match('/(\d{4})-(\d{2})-(\d{2})\s(\d{2}):(\d{2}):(\d{2})/', $row['date_end'], $match);
                $year = (int) $match[1];
                $month = (int) $match[2] - 1; // convert to zero-index to match javascript's dates
                $day = (int) $match[3];
                $hours = (int) $match[4];
                $minutes = (int) $match[5];
                $seconds = (int) $match[6];

                $end = "new Date($year, $month, $day, $hours, $minutes, $seconds)";

                echo "['".$row['id']."', '__".$row['id'].'__ '.$row['title']."', ".$start.",   ".$end.",  null, 100, ''],";
            }
            ?>            ]);

            var options = {
                height: 400,
                weight: 100
            };

            var chart = new google.visualization.GanttChart(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
        $(document).ready(function(){

            $('a').click(function(e){
                e.preventDefault();
                console.log(e.currentTarget);
                console.log(this);

                $.ajax({
                    url: $(this).attr('href'),
                    success: function(){
                        setTimeout(window.location.reload(),2000);

                    }
                });

            });

        });

    </script>

</head>
<body>
<div id="chart_div"></div>

<div>
    <a href="setDate.php?id=1&date=2015-09-02%2010:00:00&action=start">Task1 сменить время старта на 2015-09-02 10:00:00</a><br />
    <a href="setDate.php?id=1&date=2015-10-07%2010:00:00&action=start">Task1 сменить время старта на 2015-10-07 10:00:00</a><br />

    <a href="setDate.php?id=6&date=2015-10-12%2010:00:00&action=start">Task6 сменить время старта на 2015-10-12 10:00:00</a><br />
    <a href="setDate.php?id=6&date=2015-10-17%2010:00:00&action=start">Task6 сменить время старта на 2015-10-17 10:00:00</a>

    </div>


    </div><br> <hr />


    Задачи связаные с задачей 6:<br />
    <?php
        getDependsItem(6);
    ?>
</div>
<br />
<div>
    Задачи связаные с задачей 1:<br />
    <?php
        getDependsItem(1);
    ?>
</div>

</body>
</html>