-- Task 3:
USE test;
SELECT
  a.point_id,
  a.car_id,

  min(c.dt) as dt_in,
  min(c1.dt) as dt_out
FROM address a
  LEFT JOIN coord c ON ( POW((c.lat - a.lat), 2) + POW((c.lng - a.lng), 2) <= POW(a.radius/100000, 2) 						and a.`car_id`=c.car_id )
  LEFT JOIN coord c1 ON ( POW((c1.lat - a.lat), 2) + POW((c1.lng - a.lng), 2) <= POW(a.radius/100000, 2) and c1.dt>c.dt	and a.`car_id`=c1.car_id )
group by point_id, a.car_id
ORDER BY a.car_id, point_id, c.dt, c1.dt