<?php

/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 30.10.15
 * Time: 7:51
 */
abstract class Shape
{

    protected $params;
    protected $canvas;


    public function __construct($params)
    {
        $this->canvas = imagecreatetruecolor(200, 200);
        $this->params = $params;
        $this->color = imagecolorallocate($this->canvas, $this->params['r'], $this->params['g'], $this->params['b']);
    }


    abstract protected function draw();

    public function show()
    {
        header('Content-Type: image/jpeg');

        imagejpeg($this->canvas);
        imagedestroy($this->canvas);

    }

}

class ShapeCircle extends Shape
{

    public function draw()
    {
        imagefilledellipse($this->canvas, 100, 100, 150, 150, $this->color);
    }
}


class ShapeSquare extends Shape
{

    public function draw()
    {
        imagerectangle($this->canvas, 50, 50, 150, 150, $this->color);
    }
}