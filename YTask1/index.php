<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 30.10.15
 * Time: 5:38
 */

$shapes = [
    ['type' => 'circle', 'params' => [
        'r' => '216',
        'g' => '40',
        'b' => '40'

    ]],

    ['type' => 'square', 'params' => [
        'r' => '120',
        'g' => '255',
        'b' => '0'
    ]],

    ['type' => 'square', 'params' => [
        'r' => '60',
        'g' => '60',
        'b' => '222'
    ]],

    ['type' => 'wrongType', 'params' => [
        'r' => '216',
        'g' => '0',
        'b' => '0'

    ]],

    ['type' => 'circle', 'params' => [
        'r' => '0',
        'g' => '90',
        'b' => '90'

    ]],
];

require 'model.php';


//Controller
foreach ($shapes as $shape) {
    $className = 'Shape' . ucfirst($shape['type']);
    if (class_exists($className)) {
        echo '<img src="/YTask1/img.php?' . http_build_query(array('shape' => $shape)) . '" />';
    } else {
        echo '<img src="/YTask1/error.png" width="200" />';

    }
}