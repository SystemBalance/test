<?php
require 'model.php';
if (isset($_GET['shape']) && !empty($_GET['shape']['type']) && !empty($_GET['shape']['params'])) {
    $className = 'Shape' . ucfirst($_GET['shape']['type']);
    if (class_exists($className)) {
        $shapeObject = new $className($_GET['shape']['params']);
        $shapeObject->draw();
        $shapeObject->show();
    }
}